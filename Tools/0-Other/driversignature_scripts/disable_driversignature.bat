:: Permanently disable driver signature enforcement verification for on Windows 8, 8.1 and 10
:: (driver signature enforcement remains disabled even after a reboot)

@echo off
echo DISABLE_DRIVERSIGNATURE.BAT Disable driver signature enforcement by BNDias for GoPrawn.com
echo.

bcdedit.exe -set loadoptions DDISABLE_INTEGRITY_CHECKS
bcdedit.exe -set TESTSIGNING ON

:: (reboot prompt)
:PROMPT
set /p input=Are you sure you want to restart your computer now? [Y/N] 
if /i "%input%" EQU "y" goto REBOOT
if /i "%input%" EQU "n" goto :EXITBAT
goto PROMPT

:REBOOT
shutdown.exe /r /t 00
exit

:EXITBAT
echo Press any key to exit...
pause>nul
exit