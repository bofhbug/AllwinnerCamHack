:: Enable driver signature enforcement verification for on Windows 8, 8.1 and 10

@echo off
echo ENABLE_DRIVERSIGNATURE.BAT Enable driver signature enforcement by BNDias for GoPrawn.com
echo.

bcdedit.exe -set loadoptions DENABLE_INTEGRITY_CHECKS
bcdedit.exe -set TESTSIGNING OFF

:: (reboot prompt)
:PROMPT
set /p input=Are you sure you want to restart your computer now? [Y/N] 
if /i "%input%" EQU "y" goto REBOOT
if /i "%input%" EQU "n" goto EXITBAT
goto PROMPT

:REBOOT
shutdown.exe /r /t 00
exit

:EXITBAT
echo Press any key to exit...
pause>nul
exit