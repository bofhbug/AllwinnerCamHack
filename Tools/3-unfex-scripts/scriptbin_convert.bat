::SCRIPTBIN_CONVERT script by petesimon.
::Convert hardware configuration file of the camera into text file.
::Check GoPrawn.com for details.

@echo off
echo SCRIPTBIN_CONVERT.BAT Convert script.bin configuration file by petesimon for GoPrawn.com
echo.

if not exist "%~dp0script.bin"  goto SCRIPTBINFOUND
if not exist "%~dp0Tools\unscript.jar"  goto UNSCRIPTFOUND
if exist "%~dp0script.fex" (
	if exist "%~dp0script.fex.bak"  del /F "%~dp0script.fex.bak"
	ren "%~dp0script.fex" "script.fex.bak">nul
	echo Existing SCRIPT.FEX file renamed to SCRIPT.FEX.BAK
)

where java.exe > NUL
if %ERRORLEVEL% GTR 0  goto JAVANOTFOUND

echo .
echo Converting binary "script.bin" hardware description data to readable text...

java -jar "%~dp0Tools\unscript.jar" "%~dp0\script.bin" 2>NUL| more > "%~dp0script.fex"

start notepad "%~dp0script.fex"
echo Done. Check SCRIPT.FEX file.

:EXITBAT
echo Press any key to exit...
pause>nul
exit /b


:SCRIPTBINFOUND
echo SCRIPT.BIN hardware configuration file not found.
goto EXITBAT

:UNSCRIPTFOUND
echo UNSCRIPT.JAR tool not found.
goto EXITBAT

:JAVANOTFOUND
echo Java executable not installed or not found - Please download it from: https://www.java.com
goto EXITBAT